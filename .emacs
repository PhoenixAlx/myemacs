
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.


(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  (add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
  ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives '("gnu" . (concat proto "://elpa.gnu.org/packages/")))))


(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(use-package openwith
  :ensure t)
(use-package tabbar
  :ensure t)

(ivy-mode 1)
(setq ivy-use-virtual-buffers t)
(setq ivy-count-format "(%d/%d) ")
(global-set-key (kbd "C-s") 'swiper)
(global-set-key (kbd "M-x") 'counsel-M-x)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(global-set-key (kbd "<f1> f") 'counsel-describe-function)
(global-set-key (kbd "<f1> v") 'counsel-describe-variable)
(global-set-key (kbd "<f1> l") 'counsel-find-library)
(global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
(global-set-key (kbd "<f2> u") 'counsel-unicode-char)
(global-set-key (kbd "C-c g") 'counsel-git)
(global-set-key (kbd "C-c j") 'counsel-git-grep)
(global-set-key (kbd "C-c k") 'counsel-ag)
(global-set-key (kbd "C-x l") 'counsel-locate)
(global-set-key (kbd "C-S-o") 'counsel-rhythmbox)
(global-set-key (kbd "C-c C-r") 'ivy-resume)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(blink-cursor-mode nil)
 '(cua-mode t nil (cua-base))
 '(custom-enabled-themes (quote (manoj-dark)))
 '(custom-safe-themes
   (quote
    ("1c082c9b84449e54af757bcae23617d11f563fc9f33a832a8a2813c4d7dfb652" "b54826e5d9978d59f9e0a169bbd4739dd927eead3ef65f56786621b53c031a7c" "fe666e5ac37c2dfcf80074e88b9252c71a22b6f5d2f566df9a7aa4f9bea55ef8" "2540689fd0bc5d74c4682764ff6c94057ba8061a98be5dd21116bf7bf301acfb" "57f95012730e3a03ebddb7f2925861ade87f53d5bbb255398357731a7b1ac0e0" "462d6915a7eac1c6f00d5acd8b08ae379e12db2341e7d3eac44ff7f984a5e579" "356e5cbe0874b444263f3e1f9fffd4ae4c82c1b07fe085ba26e2a6d332db34dd" "93a0885d5f46d2aeac12bf6be1754faa7d5e28b27926b8aa812840fe7d0b7983" "75d3dde259ce79660bac8e9e237b55674b910b470f313cdf4b019230d01a982a" "151bde695af0b0e69c3846500f58d9a0ca8cb2d447da68d7fbf4154dcf818ebc" "d1b4990bd599f5e2186c3f75769a2c5334063e9e541e37514942c27975700370" "6b2636879127bf6124ce541b1b2824800afc49c6ccd65439d6eb987dbf200c36" "4697a2d4afca3f5ed4fdf5f715e36a6cac5c6154e105f3596b44a4874ae52c45" "b35a14c7d94c1f411890d45edfb9dc1bd61c5becd5c326790b51df6ebf60f402" "3a3de615f80a0e8706208f0a71bbcc7cc3816988f971b6d237223b6731f91605" "f0dc4ddca147f3c7b1c7397141b888562a48d9888f1595d69572db73be99a024" "a3fa4abaf08cc169b61dea8f6df1bbe4123ec1d2afeb01c17e11fdc31fc66379" "d2e9c7e31e574bf38f4b0fb927aaff20c1e5f92f72001102758005e53d77b8c9" "76dc63684249227d64634c8f62326f3d40cdc60039c2064174a7e7a7a88b1587" "a0feb1322de9e26a4d209d1cfa236deaf64662bb604fa513cca6a057ddf0ef64" "ab04c00a7e48ad784b52f34aa6bfa1e80d0c3fcacc50e1189af3651013eb0d58" "04dd0236a367865e591927a3810f178e8d33c372ad5bfef48b5ce90d4b476481" "862a0ccc73c12df4df325427f9285fa6a5bbba593a77257f43b01c84269f51b0" "47ec21abaa6642fefec1b7ace282221574c2dd7ef7715c099af5629926eb4fd7" "7356632cebc6a11a87bc5fcffaa49bae528026a78637acd03cae57c091afd9b9" "d1cc05d755d5a21a31bced25bed40f85d8677e69c73ca365628ce8024827c9e3" "bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476" "fa2b58bb98b62c3b8cf3b6f02f058ef7827a8e497125de0254f56e373abee088" default)))
 '(diary-entry-marker (quote font-lock-variable-name-face))
 '(emms-mode-line-icon-image-cache
   (quote
    (image :type xpm :ascent center :data "/* XPM */
static char *note[] = {
/* width height num_colors chars_per_pixel */
\"    10   11        2            1\",
/* colors */
\". c #1fb3b3\",
\"# c None s None\",
/* pixels */
\"###...####\",
\"###.#...##\",
\"###.###...\",
\"###.#####.\",
\"###.#####.\",
\"#...#####.\",
\"....#####.\",
\"#..######.\",
\"#######...\",
\"######....\",
\"#######..#\" };")))
 '(fci-rule-color "#222222")
 '(frame-brackground-mode (quote dark))
 '(global-display-line-numbers-mode t)
 '(gnus-logo-colors (quote ("#528d8d" "#c0c0c0")) t)
 '(gnus-mode-line-image-cache
   (quote
    (image :type xpm :ascent center :data "/* XPM */
static char *gnus-pointer[] = {
/* width height num_colors chars_per_pixel */
\"    18    13        2            1\",
/* colors */
\". c #1fb3b3\",
\"# c None s None\",
/* pixels */
\"##################\",
\"######..##..######\",
\"#####........#####\",
\"#.##.##..##...####\",
\"#...####.###...##.\",
\"#..###.######.....\",
\"#####.########...#\",
\"###########.######\",
\"####.###.#..######\",
\"######..###.######\",
\"###....####.######\",
\"###..######.######\",
\"###########.######\" };")) t)
 '(jdee-db-active-breakpoint-face-colors (cons "#000000" "#fd971f"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#000000" "#b6e63e"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#000000" "#525254"))
 '(line-number-mode nil)
 '(org-super-agenda-mode t)
 '(package-selected-packages
   (quote
    (docker xkcd google-translate svg-clock projectile ibuffer-vc company org-alert wttrin legalese org-easy-img-insert csv-mode afternoon-theme atom-one-dark-theme dark-mint-theme doom-themes atom-dark-theme madhat2r-theme gruber-darker-theme alect-themes cyberpunk-theme calfw-org calfw docker-compose-mode daemons prodigy gnuplot org-mind-map dired-ranger org-edna ipcalc counsel org-ref emacsshot auto-indent-mode format-all auto-complete py-autopep8 with-editor magit ace-window use-package neotree pdf-tools spacemacs-theme tabbar python)))
 '(save-place t)
 '(size-indication-mode t)
 '(tabbar-separator (quote (0.5)))
 '(vc-annotate-background "#222222")
 '(vc-annotate-color-map
   (quote
    ((20 . "#fa5151")
     (40 . "#ea3838")
     (60 . "#f8ffa0")
     (80 . "#e8e815")
     (100 . "#fe8b04")
     (120 . "#e5c900")
     (140 . "#32cd32")
     (160 . "#8ce096")
     (180 . "#7fb07f")
     (200 . "#3cb370")
     (220 . "#099709")
     (240 . "#2fdbde")
     (260 . "#1fb3b3")
     (280 . "#8cf1f1")
     (300 . "#94bff3")
     (320 . "#62b6ea")
     (340 . "#30a5f5")
     (360 . "#e353b9"))))
 '(vc-annotate-very-old-color "#e353b9"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; Cargar org-mouse
(require 'org-mouse)
;;(autoload 'linum-mode "linum" "toggle line numbers on/off" t)
;;(global-set-key (kbd "C-<f5>") 'linum-mode)

;; Show all line numbering by default (you can turn this off if you would like)
;;(line-number-mode 1)
;;(linum-mode 1)

(when (require 'tabbar nil t)
  ;; Tabbar settings
  (set-face-attribute
   'tabbar-default nil
   :background "gray20"
   :foreground "gray20"
   :box '(:line-width 1 :color "gray20" :style nil))
  (set-face-attribute
   'tabbar-unselected nil
   :background "gray30"
   :foreground "white"
   :box '(:line-width 5 :color "gray30" :style nil))
  (set-face-attribute
   'tabbar-selected nil
   :background "gray75"
   :foreground "black"
   :box '(:line-width 5 :color "gray75" :style nil))
  (set-face-attribute
   'tabbar-highlight nil
   :background "white"
   :foreground "black"
   :underline nil
   :box '(:line-width 5 :color "white" :style nil))
  (set-face-attribute
   'tabbar-button nil
   :background "white"
   :foreground "black"
   :box '(:line-width 1 :color "gray20" :style nil))
  (set-face-attribute
   'tabbar-separator nil
   :background "gray20"
   :height 0.6)

  ;; Change padding of the tabs
  ;; we also need to set separator to avoid overlapping tabs by highlighted tabs
  (custom-set-variables
   '(tabbar-separator (quote (0.5))))
  ;; adding spaces
  (defun tabbar-buffer-tab-label (tab)
    "Return a label for TAB.
That is, a string used to represent it on the tab bar."
    (let ((label  (if tabbar--buffer-show-groups
                      (format "[%s]  " (tabbar-tab-tabset tab))
                    (format "%s  " (tabbar-tab-value tab)))))
      ;; Unless the tab bar auto scrolls to keep the selected tab
      ;; visible, shorten the tab label to keep as many tabs as possible
      ;; in the visible area of the tab bar.
      (if tabbar-auto-scroll-flag
          label
	(tabbar-shorten
	 label (max 1 (/ (window-width)
			 (length (tabbar-view
                                  (tabbar-current-tabset)))))))))

  (tabbar-mode 1)
  (set-face-attribute
   'tabbar-default nil
   :background "gray20"
   :foreground "gray20"
   :box '(:line-width 1 :color "gray20" :style nil)
   :height 1.1)
  )



(global-superword-mode)
;; Files association
					; Openwith

(provide 'openwith)
(openwith-mode t)
(setq openwith-associations
      (list (list (openwith-make-extension-regexp '("pdf2"))
		  "okular" '(file))
	    (list (openwith-make-extension-regexp '("flac" "mp3" "wav"))
		  "vlc" '(file))
	    (list (openwith-make-extension-regexp '("avi" "flv" "mov" "mp4"
						    "mpeg" "mpg" "ogg" "wmv"))
		  "vlc" '(file))
	    (list (openwith-make-extension-regexp '("doc" "docx" "odt"))
		  "libreoffice" '("--writer" file))
	    (list (openwith-make-extension-regexp '("ods" "xls" "xlsx" "tsv" ))
		  "libreoffice" '("--calc" file))
	    (list (openwith-make-extension-regexp '("odp" "pps" "ppt" "pptx"))
		  "libreoffice" '("--impress" file))
	    ))
(desktop-save-mode 1)
(eval-after-load "dired-aux"
  '(add-to-list 'dired-compress-file-suffixes
                '("\\.zip\\'" ".zip" "unzip")))

(require 'neotree)
(global-set-key [f8] 'neotree-toggle)


(require 'org)
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)

(setq org-log-done t)
(setq org-link-frame-setup '((file . find-file)))


;; Modo indentado
(setq org-startup-indented t)
(require 'py-autopep8)
(add-hook 'python-mode-hook 'py-autopep8-enable-on-save)
;;keys
(require 'python)
(define-key python-mode-map (kbd "C->")   'python-indent-shift-right)
(define-key python-mode-map (kbd "C-<") 'python-indent-shift-left)

(require 'org-ref)
(setq org-latex-pdf-process
      '("latexmk -pdflatex='pdflatex -interaction nonstopmode' -pdf -bibtex -f %f"))


;;(global-visual-line-mode t)
(require 'calfw-org)
(add-hook 'before-save-hook 'delete-trailing-whitespace)

(setq org-agenda-overriding-columns-format "%15CATEGORY %40ITEM %TODO %SCHEDULED %DEADLINE %CLOSED ")
(defun org-agenda-show-columns (&optional arg)
  (interactive "P")
  (org-agenda arg "a")
  (org-agenda-columns)
  )

(global-set-key [f9] 'org-agenda-show-columns )
;;(set-face-attribute 'region nil :background "yellow")
(require 'org-agenda)

(setq org-agenda-prefix-format '(
				 ;; (agenda  . " %i %-12:c%?-12t% s") ;; file name + org-agenda-entry-type
 (todo  . " %i %-200:c")
  (agenda  . "%i %-25 c%?-22t% s ")
  (timeline  . " %i %-50:c%?-22t% s")
  (tags  . " %i %-12:c")
  (search . " %i %-12:c")))
(setq org-agenda-sorting-strategy '(todo-state-down scheduled-up category-up   ))


;;(setq org-icalendar-include-todo '(all))
;;(setq org-icalendar-use-scheduled '(event-if-todo event-if-not-todo))
;;(setq org-icalendar-use-deadline '(event-if-todo event-if-not-todo))
;;(defun my-icalendar-agenda-export()
 ;; (if (string= (file-name-extension (buffer-file-name)) "org")
 ;;                         (org-icalendar-combine-agenda-files))
 ;;       )
;;(add-hook 'after-save-hook 'org-icalendar-combine-agenda-files)
(setq debug-on-error nil)

;; weather from wttr.in
(use-package wttrin
  :ensure t
  :commands (wttrin)
  :init
  (setq wttrin-default-cities '("Granada" "Las borjas del campo" "Hendon, London"))
  (setq wttrin-default-accept-language '("Accept-Language" . "en-gb")))

;; Alert agenda
(require 'alert)
(alert-define-style 'Tasks :title "Alerts"
                    :notifier
                    (lambda (info)
                      ;; The message text is :message
                      (plist-get info :message)
                      ;; The :title of the alert
                      (plist-get info :title)
                      ;; The :category of the alert
                      (plist-get info :category)
                      ;; The major-mode this alert relates to
                      (plist-get info :mode)
                      ;; The buffer the alert relates to
                      (plist-get info :buffer)
                      ;; Severity of the alert.  It is one of:
                      ;;   `urgent'
                      ;;   `high'
                      ;;   `moderate'
                      ;;   `normal'
                      ;;   `low'
                      ;;   `trivial'
                      (plist-get info :severity)
                      ;; Whether this alert should persist, or fade away
                      (plist-get info :persistent)
                      ;; Data which was passed to `alert'.  Can be
                      ;; anything.
                      (plist-get info :data))

                    ;; Removers are optional.  Their job is to remove


		    ;; the visual or auditory effect of the alert.
                    :remover
                    (lambda (info)
                      ;; It is the same property list that was passed to
                      ;; the notifier function.
                      ))

(require 'org-alert)
(setq alert-default-style 'Tasks)
(org-alert-check)

;; imenus

(define-key global-map "\C-xci" 'counsel-semantic-or-imenu)

;;ibuffers
(global-set-key (kbd "C-x C-b") 'ibuffer) ;; Use Ibuffer for Buffer List
(setq ibuffer-saved-filter-groups
      '(("home"
	 ("Dired" (or (mode . dired-mode)))
	 ("Org" (or (mode . org-mode)
		    (filename . "OrgMode")))
	 ("Agenda" (or (mode . org-agenda-mode)
		    (mode . calendar-mode)))
	 ("Python" (or (mode . python-mode)
		    ))
	 ("Shells" (or (mode . shell-mode)
		    ))
	 ("Web Dev" (or (mode . html-mode)
			(mode . css-mode)
			(filename . ".html")
			))
	 ("emacs-config" (or (filename . ".emacs.d")
			     (filename . ".emacs")
			     (filename . "emacs-config")))

	 ("Dockers" (or (mode . docker-compose-mode)
                        (filename . "Dockerfile")
			))
         ))
      )
(add-hook 'ibuffer-mode-hook
	  '(lambda ()
	     (ibuffer-switch-to-saved-filter-groups "home")))
;; catala hour

(defun hores_catala_message ()
  "Write the hores in catala"
  (interactive)
  (setq hores_text (format-time-string "%H"))
  (setq minutes_text (format-time-string "%M"))
  (setq hores (string-to-number hores_text))
  (setq minutes (string-to-number minutes_text))
  (setq hores_updated (+ hores 1))
  (setq hores_updated (if (> hores_updated 23) 0 hores_updated ))
  (setq hores_table
      #s(hash-table
         size 24
         test equal
         data (
               0 "dotze"
               1 "una"
               2 "dues"
               3 "tres"
               4 "quatre"
               5 "cinc"
               6 "sis"
               7 "set"
               8 "vuit"
               9 "nou"
               10 "deu"
               11 "onze"
               12 "dotze"
               13 "una"
               14 "dues"
               15 "tres"
               16 "quatre"
               17 "cinc"
               18 "sis"
               19 "set"
               20 "vuit"
               21 "nou"
               22 "deu"
               23 "onze"
	       )
	 )
      )
  (setq parts_dia_table
      #s(hash-table
         size 24
         test equal
         data (
               0 "de la nit"
               1 "de la matinada"
               2 "de la matinada"
               3 "de la matinada"
               4 "de la matinada"
               5 "de la matinada"
               6 "del matí"
               7 "del matí"
               8 "del matí"
               9 "del matí"
               10 "del matí"
               11 "del matí"
               12 "del migdia"
               13 "del migdia"
               14 "del migdia"
               15 "de la tarda"
               16 "de la tarda"
               17 "de la tarda"
               18 "de la tarda"
               19 "del vespre"
               20 "del vespre"
               21 "del vespre"
               22 "del vespre"
               23 "de la nit"
	       )
	 )
      )
   (setq quarts_table
      #s(hash-table
         size 3
         test equal
         data (
	       0 ""
               1 "un quart"
               2 "dos quarts"
               3 "tres quarts"
	       )
	 )
      )



  (setq  hores_article (if (= hores_updated 1) " la " " " ))
  (setq  hores_article (if (= hores_updated 13) " la " " " ))
  (setq  tocat (if (= (/ minutes 15) 1) "tocat" "tocats" ))
  (setq  quartes_i (if (= (/ minutes 15) 0) "" "i" ))
  (if (= minutes 0)
    (message "%s %s en punt %s" hores_article (gethash hores hores_table ) (gethash hores parts_dia_table ) )
    ;;quarts o no quarts
    (if (= (% minutes 15) 0)
	;;quarts
	(message "%s de%s %s" (gethash (/ minutes 15)  quarts_table) (gethash hores_updated hores_table ) (gethash hores parts_dia_table ))
      ;; no quarts
      ;;mig quarts o no mig quarts
      (if (= (% minutes 15) 7)
	;;mig es 7 or not
	(if (= minutes 7)
	      (message "mig quart de%s%s %s" hores_article (gethash hores_updated hores_table )  (gethash hores_updated parts_dia_table ))
    	      (message " %s i mig de%s%s %s" (gethash  (/ minutes 15) quarts_table )  hores_article (gethash hores_updated hores_table ) (gethash hores_updated parts_dia_table ))
	)
	;;not mig
	(if (< (- (% minutes 15) 7) 0)
	    ;;not mig
	    (if (> (- (% minutes 15) 7) 3)
		;; be tocades
		(message " %s  ben %s de%s%s %s" (gethash  (/ minutes 15) quarts_table ) tocat hores_article (gethash hores_updated hores_table )  (gethash hores_updated parts_dia_table ))
	      ;; tocades
	      (message " %s %s de%s%s %s" (gethash  (/ minutes 15) quarts_table ) tocat hores_article (gethash hores_updated hores_table ) (gethash hores_updated parts_dia_table ))
		)
	  ;; mitg
	  (if (> (- (% minutes 15) 7) 3)
		;; be tocades
		(message " %s %s mig ben %s de%s%s %s" (gethash  (/ minutes 15) quarts_table ) quartes_i tocat hores_article (gethash hores_updated hores_table ) (gethash hores_updated parts_dia_table ))
	      ;; tocades
	      (message " %s %s mig %s de%s%s %s" (gethash  (/ minutes 15) quarts_table ) quartes_i tocat hores_article (gethash hores_updated hores_table )  (gethash hores_updated parts_dia_table ))

	      )
        )

       )
      )
    )
  )
;; templates org-mode

(define-skeleton Scrum-synchronizate-meeting
  "Scrum synchronizate meeting"
  ""
  "** "  (insert (format-time-string "%d/%m/%Y %A"))  "\n"
  "*** TODO synchronization meeting\n"
  "**** What have I done since the last synchronization meeting?\n"
  "**** What will I do from this moment?\n"
  "**** What impediments do I have or will I have?\n"
)
(define-skeleton Org-header-skeleton
  "Header for orgs files"
  ""
  "#+TITLE: PUT TITLE \n"
  "#+latex_header: \\hypersetup{colorlinks=true} \n"
  "#+LATEX_CLASS_OPTIONS: [a4paper, 11pt, colorlinks=true, citecolor=., linkcolor=black, urlcolor=black] \n"
  "#+LaTeX_HEADER: \\usepackage{natbib} \n"
)
